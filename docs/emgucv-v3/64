<Type Name="RotationMatrix2D" FullName="Emgu.CV.RotationMatrix2D">
  <TypeSignature Language="C#" Value="public class RotationMatrix2D : Emgu.CV.Mat" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>Emgu.CV.Mat</BaseTypeName>
  </Base>
  <Interfaces />
  <Docs>
    <summary>
             A (2x3) 2D rotation matrix. This Matrix defines an Affine Transform
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public RotationMatrix2D ();" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters />
      <Docs>
        <summary>
             Create an empty (2x3) 2D rotation matrix
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public RotationMatrix2D (System.Drawing.PointF center, double angle, double scale);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="center" Type="System.Drawing.PointF" />
        <Parameter Name="angle" Type="System.Double" />
        <Parameter Name="scale" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="center">Center of the rotation in the source image</param>
        <param name="angle">The rotation angle in degrees. Positive values mean couter-clockwise rotation (the coordiate origin is assumed at top-left corner). </param>
        <param name="scale">Isotropic scale factor.</param>
        <summary>
             Create a (2x3) 2D rotation matrix
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Clone">
      <MemberSignature Language="C#" Value="public Emgu.CV.RotationMatrix2D Clone ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.RotationMatrix2D</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Return a clone of the Matrix
             </summary>
        <returns>A clone of the Matrix</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="CreateRotationMatrix">
      <MemberSignature Language="C#" Value="public static Emgu.CV.RotationMatrix2D CreateRotationMatrix (System.Drawing.PointF center, double angle, System.Drawing.Size srcImageSize, out System.Drawing.Size dstImageSize);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.RotationMatrix2D</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="center" Type="System.Drawing.PointF" />
        <Parameter Name="angle" Type="System.Double" />
        <Parameter Name="srcImageSize" Type="System.Drawing.Size" />
        <Parameter Name="dstImageSize" Type="System.Drawing.Size&amp;" RefType="out" />
      </Parameters>
      <Docs>
        <param name="center">The rotation center</param>
        <param name="angle">The rotation angle in degrees. Positive values mean couter-clockwise rotation (the coordiate origin is assumed at image centre). </param>
        <param name="srcImageSize">The source image size</param>
        <param name="dstImageSize">The minimun size of the destination image</param>
        <summary>
             Create a rotation matrix for rotating an image
             </summary>
        <returns>The rotation matrix that rotate the source image to the destination image.</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotateLines">
      <MemberSignature Language="C#" Value="public void RotateLines (Emgu.CV.Structure.LineSegment2DF[] lineSegments);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="lineSegments" Type="Emgu.CV.Structure.LineSegment2DF[]" />
      </Parameters>
      <Docs>
        <param name="lineSegments">The line segments to be rotated</param>
        <summary>
             Rotate the <paramref name="lineSegments" />, the value of the input <paramref name="lineSegments" /> will be changed.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotatePoints">
      <MemberSignature Language="C#" Value="public void RotatePoints (Emgu.CV.Structure.MCvPoint2D64f[] points);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="points" Type="Emgu.CV.Structure.MCvPoint2D64f[]" />
      </Parameters>
      <Docs>
        <param name="points">The points to be rotated, its value will be modified</param>
        <summary>
             Rotate the <paramref name="points" />, the value of the input <paramref name="points" /> will be changed.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotatePoints">
      <MemberSignature Language="C#" Value="public void RotatePoints (System.Drawing.PointF[] points);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="points" Type="System.Drawing.PointF[]" />
      </Parameters>
      <Docs>
        <param name="points">The points to be rotated, its value will be modified</param>
        <summary>
             Rotate the <paramref name="points" />, the value of the input <paramref name="points" /> will be changed.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotatePoints&lt;TDepth&gt;">
      <MemberSignature Language="C#" Value="public void RotatePoints&lt;TDepth&gt; (Emgu.CV.Matrix&lt;TDepth&gt; points) where TDepth : new();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <TypeParameters>
        <TypeParameter Name="TDepth">
          <Constraints>
            <ParameterAttribute>DefaultConstructorConstraint</ParameterAttribute>
          </Constraints>
        </TypeParameter>
      </TypeParameters>
      <Parameters>
        <Parameter Name="points" Type="Emgu.CV.Matrix&lt;TDepth&gt;" />
      </Parameters>
      <Docs>
        <typeparam name="TDepth">The depth of the points, must be double or float</typeparam>
        <param name="points">The N 2D-points to be rotated</param>
        <summary>
             Rotate the single channel Nx2 matrix where N is the number of 2D points. The value of the matrix is changed after rotation.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="SetRotation">
      <MemberSignature Language="C#" Value="public void SetRotation (System.Drawing.PointF center, double angle, double scale);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="center" Type="System.Drawing.PointF" />
        <Parameter Name="angle" Type="System.Double" />
        <Parameter Name="scale" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="center">Center of the rotation in the source image</param>
        <param name="angle">The rotation angle in degrees. Positive values mean couter-clockwise rotation (the coordiate origin is assumed at top-left corner). </param>
        <param name="scale">Isotropic scale factor.</param>
        <summary>
             Set the values of the rotation matrix
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
