<Type Name="SelectiveSearchSegmentation" FullName="Emgu.CV.XImgproc.SelectiveSearchSegmentation">
  <TypeSignature Language="C#" Value="public class SelectiveSearchSegmentation : Emgu.Util.UnmanagedObject" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>Emgu.Util.UnmanagedObject</BaseTypeName>
  </Base>
  <Interfaces />
  <Docs>
    <summary>
             Selective search segmentation algorithm The class implements the algorithm described in:
             Jasper RR Uijlings, Koen EA van de Sande, Theo Gevers, and Arnold WM Smeulders. Selective search for object recognition. International journal of computer vision, 104(2):154–171, 2013.
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public SelectiveSearchSegmentation ();" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters />
      <Docs>
        <summary>
             Selective search segmentation algorithm
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="AddImage">
      <MemberSignature Language="C#" Value="public void AddImage (Emgu.CV.IInputArray img);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="img" Type="Emgu.CV.IInputArray" />
      </Parameters>
      <Docs>
        <param name="img">	The image</param>
        <summary>
             Add a new image in the list of images to process.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="DisposeObject">
      <MemberSignature Language="C#" Value="protected override void DisposeObject ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Release the unmanaged memory associated with this object.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Process">
      <MemberSignature Language="C#" Value="public System.Drawing.Rectangle[] Process ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Drawing.Rectangle[]</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Based on all images, graph segmentations and stragies, computes all possible rects and return them.
             </summary>
        <returns>	The list of rects. The first ones are more relevents than the lasts ones.</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="SetBaseImage">
      <MemberSignature Language="C#" Value="public void SetBaseImage (Emgu.CV.IInputArray image);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="image" Type="Emgu.CV.IInputArray" />
      </Parameters>
      <Docs>
        <param name="image">The image</param>
        <summary>
             Set a image used by switch* functions to initialize the class.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="SwitchToSelectiveSearchFast">
      <MemberSignature Language="C#" Value="public void SwitchToSelectiveSearchFast (int baseK, int incK, float sigma);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="baseK" Type="System.Int32" />
        <Parameter Name="incK" Type="System.Int32" />
        <Parameter Name="sigma" Type="System.Single" />
      </Parameters>
      <Docs>
        <param name="baseK">The k parameter for the first graph segmentation</param>
        <param name="incK">The increment of the k parameter for all graph segmentations</param>
        <param name="sigma">The sigma parameter for the graph segmentation</param>
        <summary>
             Initialize the class with the 'Selective search fast' parameters
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="SwitchToSelectiveSearchQuality">
      <MemberSignature Language="C#" Value="public void SwitchToSelectiveSearchQuality (int baseK, int incK, float sigma);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="baseK" Type="System.Int32" />
        <Parameter Name="incK" Type="System.Int32" />
        <Parameter Name="sigma" Type="System.Single" />
      </Parameters>
      <Docs>
        <param name="baseK">The k parameter for the first graph segmentation</param>
        <param name="incK">The increment of the k parameter for all graph segmentations</param>
        <param name="sigma">The sigma parameter for the graph segmentation</param>
        <summary>
             Initialize the class with the 'Selective search quality' parameters
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="SwitchToSingleStrategy">
      <MemberSignature Language="C#" Value="public void SwitchToSingleStrategy (int k, float sigma);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="k" Type="System.Int32" />
        <Parameter Name="sigma" Type="System.Single" />
      </Parameters>
      <Docs>
        <param name="k">The k parameter for the graph segmentation</param>
        <param name="sigma">The sigma parameter for the graph segmentation</param>
        <summary>
             Initialize the class with the 'Single stragegy' parameters
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
