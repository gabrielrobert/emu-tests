<Type Name="KalmanFilter" FullName="Emgu.CV.KalmanFilter">
  <TypeSignature Language="C#" Value="public class KalmanFilter : Emgu.Util.UnmanagedObject" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>Emgu.Util.UnmanagedObject</BaseTypeName>
  </Base>
  <Interfaces />
  <Docs>
    <summary>
             The class implements a standard Kalman filter. However, you can modify transitionMatrix, controlMatrix, and measurementMatrix to get
             an extended Kalman filter functionality.
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public KalmanFilter (int dynamParams, int measureParams, int controlParams, Emgu.CV.CvEnum.DepthType type);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="dynamParams" Type="System.Int32" />
        <Parameter Name="measureParams" Type="System.Int32" />
        <Parameter Name="controlParams" Type="System.Int32" />
        <Parameter Name="type" Type="Emgu.CV.CvEnum.DepthType" />
      </Parameters>
      <Docs>
        <param name="dynamParams">Dimensionality of the state.</param>
        <param name="measureParams">Dimensionality of the measurement.</param>
        <param name="controlParams">Dimensionality of the control vector.</param>
        <param name="type">Type of the created matrices that should be Cv32F or Cv64F</param>
        <summary>
             Initializes a new instance of the <see cref="T:Emgu.CV.KalmanFilter" /> class.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ControlMatrix">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat ControlMatrix { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Control matrix (B) (not used if there is no control)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Correct">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat Correct (Emgu.CV.Mat measurement);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="measurement" Type="Emgu.CV.Mat" />
      </Parameters>
      <Docs>
        <param name="measurement">The measured system parameters</param>
        <summary>
             Updates the predicted state from the measurement.
             </summary>
        <returns />
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="DisposeObject">
      <MemberSignature Language="C#" Value="protected override void DisposeObject ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Release the unmanaged resources
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ErrorCovPost">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat ErrorCovPost { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             posteriori error estimate covariance matrix (P(k)): P(k)=(I-K(k)*H)*P'(k)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ErrorCovPre">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat ErrorCovPre { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             priori error estimate covariance matrix (P'(k)): P'(k)=A*P(k-1)*At + Q)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Gain">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat Gain { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Kalman gain matrix (K(k)): K(k)=P'(k)*Ht*inv(H*P'(k)*Ht+R)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="MeasurementMatrix">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat MeasurementMatrix { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Measurement matrix (H)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="MeasurementNoiseCov">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat MeasurementNoiseCov { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Measurement noise covariance matrix (R)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Predict">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat Predict (Emgu.CV.Mat control);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="control" Type="Emgu.CV.Mat" />
      </Parameters>
      <Docs>
        <param name="control">The control.</param>
        <summary>
             Perform the predict operation using the option control input
             </summary>
        <returns>The predicted state. </returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ProcessNoiseCov">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat ProcessNoiseCov { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Process noise covariance matrix (Q)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="StatePost">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat StatePost { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Corrected state (x(k)): x(k)=x'(k)+K(k)*(z(k)-H*x'(k))
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="StatePre">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat StatePre { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Predicted state (x'(k)): x(k)=A*x(k-1)+B*u(k)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="TransitionMatrix">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat TransitionMatrix { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             State transition matrix (A)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
