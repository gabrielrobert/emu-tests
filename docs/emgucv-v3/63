<Type Name="Quaternions" FullName="Emgu.CV.Quaternions">
  <TypeSignature Language="C#" Value="public struct Quaternions : IEquatable&lt;Emgu.CV.Quaternions&gt;" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>System.ValueType</BaseTypeName>
  </Base>
  <Interfaces>
    <Interface>
      <InterfaceName>System.IEquatable&lt;Emgu.CV.Quaternions&gt;</InterfaceName>
    </Interface>
  </Interfaces>
  <Docs>
    <summary>
             A unit quaternions that defines rotation in 3D
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public Quaternions (double w, double x, double y, double z);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="w" Type="System.Double" />
        <Parameter Name="x" Type="System.Double" />
        <Parameter Name="y" Type="System.Double" />
        <Parameter Name="z" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="w">The W component of the quaternion: the value for cos(rotation angle / 2)</param>
        <param name="x">The X component of the vector: rotation axis * sin(rotation angle / 2)</param>
        <param name="y">The Y component of the vector: rotation axis * sin(rotation angle / 2)</param>
        <param name="z">The Z component of the vector: rotation axis * sin(rotation angle / 2)</param>
        <summary>
             Create a quaternion with the specific values
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="AxisAngle">
      <MemberSignature Language="C#" Value="public Emgu.CV.Structure.MCvPoint3D64f AxisAngle { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Structure.MCvPoint3D64f</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get or set the equivalent axis angle representation. (x,y,z) is the rotation axis and |(x,y,z)| is the rotation angle in radians
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Conjugate">
      <MemberSignature Language="C#" Value="public void Conjugate ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Compute the conjugate of the quaternions
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Empty">
      <MemberSignature Language="C#" Value="public static readonly Emgu.CV.Quaternions Empty;" />
      <MemberType>Field</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Quaternions</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get the quaternions that represent a rotation of 0 degrees.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Equals">
      <MemberSignature Language="C#" Value="public bool Equals (Emgu.CV.Quaternions other);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Boolean</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="other" Type="Emgu.CV.Quaternions" />
      </Parameters>
      <Docs>
        <param name="other">The quaternions to be compared</param>
        <summary>
             Check if this quaternions equals to <paramref name="other" /></summary>
        <returns>True if two quaternions equals, false otherwise</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetEuler">
      <MemberSignature Language="C#" Value="public void GetEuler (ref double x, ref double y, ref double z);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="x" Type="System.Double&amp;" RefType="ref" />
        <Parameter Name="y" Type="System.Double&amp;" RefType="ref" />
        <Parameter Name="z" Type="System.Double&amp;" RefType="ref" />
      </Parameters>
      <Docs>
        <param name="x">Rotation around x-axis (roll) in radian</param>
        <param name="y">Rotation around y-axis (pitch) in radian</param>
        <param name="z">rotation around z-axis (yaw) in radian</param>
        <summary>
             Get the equivalent euler angle
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetRotationMatrix">
      <MemberSignature Language="C#" Value="public void GetRotationMatrix (Emgu.CV.Matrix&lt;double&gt; rotation);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="rotation" Type="Emgu.CV.Matrix&lt;System.Double&gt;" />
      </Parameters>
      <Docs>
        <param name="rotation">The (3x3) rotation matrix which values will be set to represent this quaternions</param>
        <summary>
             Fill the (3x3) rotation matrix with the value such that it represent the quaternions
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Multiply">
      <MemberSignature Language="C#" Value="public Emgu.CV.Quaternions Multiply (Emgu.CV.Quaternions quaternionsOther);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Quaternions</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="quaternionsOther" Type="Emgu.CV.Quaternions" />
      </Parameters>
      <Docs>
        <param name="quaternionsOther">The other rotation</param>
        <summary>
             Multiply the current Quaternions with <paramref name="quaternionsOther" /></summary>
        <returns>To be added.</returns>
        <remarks>To be added.</remarks>
        <return>A composition of the two rotations</return>
      </Docs>
    </Member>
    <Member MemberName="op_Multiply">
      <MemberSignature Language="C#" Value="public static Emgu.CV.Quaternions op_Multiply (Emgu.CV.Quaternions q1, Emgu.CV.Quaternions q2);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Quaternions</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="q1" Type="Emgu.CV.Quaternions" />
        <Parameter Name="q2" Type="Emgu.CV.Quaternions" />
      </Parameters>
      <Docs>
        <param name="q1">The quaternions to be multiplied</param>
        <param name="q2">The quaternions to be multiplied</param>
        <summary>
             Computes the multiplication of two quaternions
             </summary>
        <returns>The multiplication of two quaternions</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotatePoint">
      <MemberSignature Language="C#" Value="public Emgu.CV.Structure.MCvPoint3D64f RotatePoint (Emgu.CV.Structure.MCvPoint3D64f point);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Structure.MCvPoint3D64f</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="point" Type="Emgu.CV.Structure.MCvPoint3D64f" />
      </Parameters>
      <Docs>
        <param name="point">The point to be rotated</param>
        <summary>
             Rotate the specific point and return the result
             </summary>
        <returns>The rotated point</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotatePoints">
      <MemberSignature Language="C#" Value="public void RotatePoints (Emgu.CV.Matrix&lt;double&gt; pointsSrc, Emgu.CV.Matrix&lt;double&gt; pointsDst);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="pointsSrc" Type="Emgu.CV.Matrix&lt;System.Double&gt;" />
        <Parameter Name="pointsDst" Type="Emgu.CV.Matrix&lt;System.Double&gt;" />
      </Parameters>
      <Docs>
        <param name="pointsSrc">The points to be rotated</param>
        <param name="pointsDst">The result of the rotation, should be the same size as <paramref name="pointsSrc" />, can be <paramref name="pointsSrc" /> as well for inplace rotation</param>
        <summary>
             Rotate the points in <paramref name="pointsSrc" /> and save the result in <paramref name="pointsDst" />. In-place operation is supported (<paramref name="pointsSrc" /> == <paramref name="pointsDst" />).
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotationAngle">
      <MemberSignature Language="C#" Value="public double RotationAngle { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get the rotation angle in radian
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="RotationAxis">
      <MemberSignature Language="C#" Value="public Emgu.CV.Structure.MCvPoint3D64f RotationAxis { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Structure.MCvPoint3D64f</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get the rotation axis of the quaternion
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="SetEuler">
      <MemberSignature Language="C#" Value="public void SetEuler (double x, double y, double z);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="x" Type="System.Double" />
        <Parameter Name="y" Type="System.Double" />
        <Parameter Name="z" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="x">Rotation around x-axis (roll) in radian</param>
        <param name="y">Rotation around y-axis (pitch) in radian</param>
        <param name="z">rotation around z-axis (yaw) in radian</param>
        <summary>
             Set the value of the quaternions using euler angle
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Slerp">
      <MemberSignature Language="C#" Value="public Emgu.CV.Quaternions Slerp (Emgu.CV.Quaternions quaternionsOther, double weightForOther);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Quaternions</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="quaternionsOther" Type="Emgu.CV.Quaternions" />
        <Parameter Name="weightForOther" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="quaternionsOther">The other quaternions to interpolate with</param>
        <param name="weightForOther">If 0.0, the result is the same as this quaternions. If 1.0 the result is the same as <paramref name="quaternionsOther" /></param>
        <summary>
             Perform quaternions linear interpolation
             </summary>
        <returns>The linear interpolated quaternions</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ToString">
      <MemberSignature Language="C#" Value="public override string ToString ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.String</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Get the string representation of the Quaternions
             </summary>
        <returns>The string representation</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="W">
      <MemberSignature Language="C#" Value="public double W { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             The W component of the quaternion: the value for cos(rotation angle / 2)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="X">
      <MemberSignature Language="C#" Value="public double X { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             The X component of the vector: rotation axis * sin(rotation angle / 2)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Y">
      <MemberSignature Language="C#" Value="public double Y { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             The Y component of the vector: rotation axis * sin(rotation angle / 2)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Z">
      <MemberSignature Language="C#" Value="public double Z { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             The Z component of the vector: rotation axis * sin(rotation angle / 2)
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
