<Type Name="CameraCalibration" FullName="Emgu.CV.CameraCalibration">
  <TypeSignature Language="C#" Value="public static class CameraCalibration" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>System.Object</BaseTypeName>
  </Base>
  <Interfaces />
  <Attributes>
    <Attribute>
      <AttributeName>System.Obsolete("This class will be removed in the next release. Please uses the corresponding CvInvoke function instead.")</AttributeName>
    </Attribute>
  </Attributes>
  <Docs>
    <summary>
             Camera calibration functions
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName="CalibrateCamera">
      <MemberSignature Language="C#" Value="public static double CalibrateCamera (Emgu.CV.Structure.MCvPoint3D32f[][] objectPoints, System.Drawing.PointF[][] imagePoints, System.Drawing.Size imageSize, Emgu.CV.IntrinsicCameraParameters intrinsicParam, Emgu.CV.CvEnum.CalibType calibrationType, Emgu.CV.Structure.MCvTermCriteria termCriteria, out Emgu.CV.ExtrinsicCameraParameters[] extrinsicParams);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="objectPoints" Type="Emgu.CV.Structure.MCvPoint3D32f[][]" />
        <Parameter Name="imagePoints" Type="System.Drawing.PointF[][]" />
        <Parameter Name="imageSize" Type="System.Drawing.Size" />
        <Parameter Name="intrinsicParam" Type="Emgu.CV.IntrinsicCameraParameters" />
        <Parameter Name="calibrationType" Type="Emgu.CV.CvEnum.CalibType" />
        <Parameter Name="termCriteria" Type="Emgu.CV.Structure.MCvTermCriteria" />
        <Parameter Name="extrinsicParams" Type="Emgu.CV.ExtrinsicCameraParameters[]&amp;" RefType="out" />
      </Parameters>
      <Docs>
        <param name="objectPoints">The 3D location of the object points. The first index is the index of image, second index is the index of the point</param>
        <param name="imagePoints">The 2D image location of the points. The first index is the index of the image, second index is the index of the point</param>
        <param name="imageSize">The size of the image, used only to initialize intrinsic camera matrix</param>
        <param name="intrinsicParam">The intrisinc parameters, might contains some initial values. The values will be modified by this function.</param>
        <param name="calibrationType">cCalibration type</param>
        <param name="termCriteria">The termination criteria</param>
        <param name="extrinsicParams">The output array of extrinsic parameters.</param>
        <summary>
             Estimates intrinsic camera parameters and extrinsic parameters for each of the views
             </summary>
        <returns>The final reprojection error</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="EstimateRigidTransform">
      <MemberSignature Language="C#" Value="public static Emgu.CV.Mat EstimateRigidTransform (System.Drawing.PointF[] sourcePoints, System.Drawing.PointF[] destinationPoints, bool fullAffine);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="sourcePoints" Type="System.Drawing.PointF[]" />
        <Parameter Name="destinationPoints" Type="System.Drawing.PointF[]" />
        <Parameter Name="fullAffine" Type="System.Boolean" />
      </Parameters>
      <Docs>
        <param name="sourcePoints">The points from the source image</param>
        <param name="destinationPoints">The corresponding points from the destination image</param>
        <param name="fullAffine">Indicates if full affine should be performed</param>
        <summary>
             Estimate rigid transformation between 2 point sets.
             </summary>
        <returns>If success, the 2x3 rotation matrix that defines the Affine transform. Otherwise null is returned.</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetAffineTransform">
      <MemberSignature Language="C#" Value="public static Emgu.CV.Mat GetAffineTransform (System.Drawing.PointF[] src, System.Drawing.PointF[] dest);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="src" Type="System.Drawing.PointF[]" />
        <Parameter Name="dest" Type="System.Drawing.PointF[]" />
      </Parameters>
      <Docs>
        <param name="src">Coordinates of 3 triangle vertices in the source image. If the array contains more than 3 points, only the first 3 will be used</param>
        <param name="dest">Coordinates of the 3 corresponding triangle vertices in the destination image. If the array contains more than 3 points, only the first 3 will be used</param>
        <summary>
             Calculates the matrix of an affine transform such that:
             (x'_i,y'_i)^T=map_matrix (x_i,y_i,1)^T
             where dst(i)=(x'_i,y'_i), src(i)=(x_i,y_i), i=0..2.
             </summary>
        <returns>The 2x3 rotation matrix that defines the Affine transform</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ProjectPoints">
      <MemberSignature Language="C#" Value="public static System.Drawing.PointF[] ProjectPoints (Emgu.CV.Structure.MCvPoint3D32f[] objectPoints, Emgu.CV.ExtrinsicCameraParameters extrin, Emgu.CV.IntrinsicCameraParameters intrin, Emgu.CV.Matrix&lt;float&gt;[] mats);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Drawing.PointF[]</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="objectPoints" Type="Emgu.CV.Structure.MCvPoint3D32f[]" />
        <Parameter Name="extrin" Type="Emgu.CV.ExtrinsicCameraParameters" />
        <Parameter Name="intrin" Type="Emgu.CV.IntrinsicCameraParameters" />
        <Parameter Name="mats" Type="Emgu.CV.Matrix&lt;System.Single&gt;[]">
          <Attributes>
            <Attribute>
              <AttributeName>System.ParamArray</AttributeName>
            </Attribute>
          </Attributes>
        </Parameter>
      </Parameters>
      <Docs>
        <param name="objectPoints">The array of object points.</param>
        <param name="extrin">Extrinsic parameters</param>
        <param name="intrin">Intrinsic parameters</param>
        <param name="mats">Optional matrix supplied in the following order: dpdrot, dpdt, dpdf, dpdc, dpddist</param>
        <summary>
             Computes projections of 3D points to the image plane given intrinsic and extrinsic camera parameters. 
             Optionally, the function computes jacobians - matrices of partial derivatives of image points as functions of all the input parameters w.r.t. the particular parameters, intrinsic and/or extrinsic. 
             The jacobians are used during the global optimization in cvCalibrateCamera2 and cvFindExtrinsicCameraParams2. 
             The function itself is also used to compute back-projection error for with current intrinsic and extrinsic parameters. 
             </summary>
        <returns>The array of image points which is the projection of <paramref name="objectPoints" /></returns>
        <remarks>Note, that with intrinsic and/or extrinsic parameters set to special values, the function can be used to compute just extrinsic transformation or just intrinsic transformation (i.e. distortion of a sparse set of points) </remarks>
      </Docs>
    </Member>
    <Member MemberName="SolvePnP">
      <MemberSignature Language="C#" Value="public static Emgu.CV.ExtrinsicCameraParameters SolvePnP (Emgu.CV.Structure.MCvPoint3D32f[] objectPoints, System.Drawing.PointF[] imagePoints, Emgu.CV.IntrinsicCameraParameters intrin, Emgu.CV.CvEnum.SolvePnpMethod method);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.ExtrinsicCameraParameters</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="objectPoints" Type="Emgu.CV.Structure.MCvPoint3D32f[]" />
        <Parameter Name="imagePoints" Type="System.Drawing.PointF[]" />
        <Parameter Name="intrin" Type="Emgu.CV.IntrinsicCameraParameters" />
        <Parameter Name="method" Type="Emgu.CV.CvEnum.SolvePnpMethod" />
      </Parameters>
      <Docs>
        <param name="objectPoints">The array of object points</param>
        <param name="imagePoints">The array of corresponding image points</param>
        <param name="intrin">The intrinsic parameters</param>
        <param name="method">Method for solving a PnP problem</param>
        <summary>
             Estimates extrinsic camera parameters using known intrinsic parameters and extrinsic parameters for each view. The coordinates of 3D object points and their correspondent 2D projections must be specified. This function also minimizes back-projection error. 
             </summary>
        <returns>The extrinsic parameters</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="StereoCalibrate">
      <MemberSignature Language="C#" Value="public static void StereoCalibrate (Emgu.CV.Structure.MCvPoint3D32f[][] objectPoints, System.Drawing.PointF[][] imagePoints1, System.Drawing.PointF[][] imagePoints2, Emgu.CV.IntrinsicCameraParameters intrinsicParam1, Emgu.CV.IntrinsicCameraParameters intrinsicParam2, System.Drawing.Size imageSize, Emgu.CV.CvEnum.CalibType flags, Emgu.CV.Structure.MCvTermCriteria termCrit, out Emgu.CV.ExtrinsicCameraParameters extrinsicParams, out Emgu.CV.Matrix&lt;double&gt; foundamentalMatrix, out Emgu.CV.Matrix&lt;double&gt; essentialMatrix);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="objectPoints" Type="Emgu.CV.Structure.MCvPoint3D32f[][]" />
        <Parameter Name="imagePoints1" Type="System.Drawing.PointF[][]" />
        <Parameter Name="imagePoints2" Type="System.Drawing.PointF[][]" />
        <Parameter Name="intrinsicParam1" Type="Emgu.CV.IntrinsicCameraParameters" />
        <Parameter Name="intrinsicParam2" Type="Emgu.CV.IntrinsicCameraParameters" />
        <Parameter Name="imageSize" Type="System.Drawing.Size" />
        <Parameter Name="flags" Type="Emgu.CV.CvEnum.CalibType" />
        <Parameter Name="termCrit" Type="Emgu.CV.Structure.MCvTermCriteria" />
        <Parameter Name="extrinsicParams" Type="Emgu.CV.ExtrinsicCameraParameters&amp;" RefType="out" />
        <Parameter Name="foundamentalMatrix" Type="Emgu.CV.Matrix&lt;System.Double&gt;&amp;" RefType="out" />
        <Parameter Name="essentialMatrix" Type="Emgu.CV.Matrix&lt;System.Double&gt;&amp;" RefType="out" />
      </Parameters>
      <Docs>
        <param name="objectPoints">The 3D location of the object points. The first index is the index of image, second index is the index of the point</param>
        <param name="imagePoints1">The 2D image location of the points for camera 1. The first index is the index of the image, second index is the index of the point</param>
        <param name="imagePoints2">The 2D image location of the points for camera 2. The first index is the index of the image, second index is the index of the point</param>
        <param name="intrinsicParam1">The intrisinc parameters for camera 1, might contains some initial values. The values will be modified by this function.</param>
        <param name="intrinsicParam2">The intrisinc parameters for camera 2, might contains some initial values. The values will be modified by this function.</param>
        <param name="imageSize">Size of the image, used only to initialize intrinsic camera matrix</param>
        <param name="flags">Different flags</param>
        <param name="termCrit">Termination criteria for the iterative optimiziation algorithm </param>
        <param name="extrinsicParams">The extrinsic parameters which contains:
             R - The rotation matrix between the 1st and the 2nd cameras' coordinate systems; 
             T - The translation vector between the cameras' coordinate systems. </param>
        <param name="foundamentalMatrix">The fundamental matrix</param>
        <param name="essentialMatrix">The essential matrix</param>
        <summary>
             Estimates transformation between the 2 cameras making a stereo pair. If we have a stereo camera, where the relative position and orientatation of the 2 cameras is fixed, and if we computed poses of an object relative to the fist camera and to the second camera, (R1, T1) and (R2, T2), respectively (that can be done with cvFindExtrinsicCameraParams2), obviously, those poses will relate to each other, i.e. given (R1, T1) it should be possible to compute (R2, T2) - we only need to know the position and orientation of the 2nd camera relative to the 1st camera. That's what the described function does. It computes (R, T) such that:
             R2=R*R1,
             T2=R*T1 + T
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
