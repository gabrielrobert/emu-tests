<Type Name="MotionHistory" FullName="Emgu.CV.MotionHistory">
  <TypeSignature Language="C#" Value="public class MotionHistory : Emgu.Util.DisposableObject" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>Emgu.Util.DisposableObject</BaseTypeName>
  </Base>
  <Interfaces />
  <Docs>
    <summary>
             The motion history class
             </summary>
    <remarks>
             For help on using this class, take a look at the Motion Detection example
             </remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public MotionHistory (double mhiDuration, double maxTimeDelta, double minTimeDelta);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="mhiDuration" Type="System.Double" />
        <Parameter Name="maxTimeDelta" Type="System.Double" />
        <Parameter Name="minTimeDelta" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="mhiDuration">In second, the duration of motion history you wants to keep</param>
        <param name="maxTimeDelta">In second. Any change happens between a time interval greater than this will not be considered</param>
        <param name="minTimeDelta">In second. Any change happens between a time interval smaller than this will not be considered.</param>
        <summary>
             Create a motion history object
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public MotionHistory (double mhiDuration, double maxTimeDelta, double minTimeDelta, DateTime startTime);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="mhiDuration" Type="System.Double" />
        <Parameter Name="maxTimeDelta" Type="System.Double" />
        <Parameter Name="minTimeDelta" Type="System.Double" />
        <Parameter Name="startTime" Type="System.DateTime" />
      </Parameters>
      <Docs>
        <param name="mhiDuration">In second, the duration of motion history you wants to keep</param>
        <param name="maxTimeDelta">In second. Any change happens between a time interval larger than this will not be considered</param>
        <param name="minTimeDelta">In second. Any change happens between a time interval smaller than this will not be considered.</param>
        <param name="startTime">The start time of the motion history</param>
        <summary>
             Create a motion history object
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="DisposeObject">
      <MemberSignature Language="C#" Value="protected override void DisposeObject ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Release unmanaged resources
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetMotionComponents">
      <MemberSignature Language="C#" Value="public void GetMotionComponents (Emgu.CV.IOutputArray segMask, Emgu.CV.Util.VectorOfRect boundingRects);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="segMask" Type="Emgu.CV.IOutputArray" />
        <Parameter Name="boundingRects" Type="Emgu.CV.Util.VectorOfRect" />
      </Parameters>
      <Docs>
        <param name="segMask">To be added.</param>
        <param name="boundingRects">To be added.</param>
        <summary>
             Get a sequence of motion component
             </summary>
        <returns>A sequence of motion components</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Mask">
      <MemberSignature Language="C#" Value="public Emgu.CV.Mat Mask { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Mat</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             The motion mask. 
             Do not dispose this image.
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="MotionInfo">
      <MemberSignature Language="C#" Value="public void MotionInfo (Emgu.CV.Mat forgroundMask, System.Drawing.Rectangle motionRectangle, out double angle, out double motionPixelCount);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="forgroundMask" Type="Emgu.CV.Mat" />
        <Parameter Name="motionRectangle" Type="System.Drawing.Rectangle" />
        <Parameter Name="angle" Type="System.Double&amp;" RefType="out" />
        <Parameter Name="motionPixelCount" Type="System.Double&amp;" RefType="out" />
      </Parameters>
      <Docs>
        <param name="forgroundMask">The foreground mask used to calculate the motion info.</param>
        <param name="motionRectangle">The rectangle area of the motion</param>
        <param name="angle">The orientation of the motion</param>
        <param name="motionPixelCount">Number of motion pixels within silhouette ROI</param>
        <summary>
             Given a rectangle area of the motion, output the angle of the motion and the number of pixels that are considered to be motion pixel 
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ReleaseManagedResources">
      <MemberSignature Language="C#" Value="protected override void ReleaseManagedResources ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Release any images associated with this object
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Update">
      <MemberSignature Language="C#" Value="public void Update (Emgu.CV.Mat image);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="image" Type="Emgu.CV.Mat" />
      </Parameters>
      <Docs>
        <param name="image">The image to be added to history</param>
        <summary>
             Update the motion history with the specific image and current timestamp
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Update">
      <MemberSignature Language="C#" Value="public void Update (Emgu.CV.Mat foregroundMask, DateTime timestamp);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="foregroundMask" Type="Emgu.CV.Mat" />
        <Parameter Name="timestamp" Type="System.DateTime" />
      </Parameters>
      <Docs>
        <param name="foregroundMask">The foreground of the image to be added to history</param>
        <param name="timestamp">The time when the image is captured</param>
        <summary>
             Update the motion history with the specific image and the specific timestamp
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
