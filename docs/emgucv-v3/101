<Type Name="Retina" FullName="Emgu.CV.Bioinspired.Retina">
  <TypeSignature Language="C#" Value="public class Retina : Emgu.Util.UnmanagedObject" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>Emgu.Util.UnmanagedObject</BaseTypeName>
  </Base>
  <Interfaces />
  <Docs>
    <summary>
             A wrapper class which allows the Gipsa/Listic Labs model to be used.
             This retina model allows spatio-temporal image processing (applied on still images, video sequences).
             As a summary, these are the retina model properties:
             1. It applies a spectral whithening (mid-frequency details enhancement);
             2. high frequency spatio-temporal noise reduction;
             3. low frequency luminance to be reduced (luminance range compression);
             4. local logarithmic luminance compression allows details to be enhanced in low light conditions.
             USE : this model can be used basically for spatio-temporal video effects but also for :
                 _using the getParvo method output matrix : texture analysiswith enhanced signal to noise ratio and enhanced details robust against input images luminance ranges
                  _using the getMagno method output matrix : motion analysis also with the previously cited properties
                  
             For more information, reer to the following papers :
             Benoit A., Caplier A., Durette B., Herault, J., "USING HUMAN VISUAL SYSTEM MODELING FOR BIO-INSPIRED LOW LEVEL IMAGE PROCESSING", Elsevier, Computer Vision and Image Understanding 114 (2010), pp. 758-773, DOI: http://dx.doi.org/10.1016/j.cviu.2010.01.011
             Vision: Images, Signals and Neural Networks: Models of Neural Processing in Visual Perception (Progress in Neural Processing),By: Jeanny Herault, ISBN: 9814273686. WAPI (Tower ID): 113266891.
            
             The retina filter includes the research contributions of phd/research collegues from which code has been redrawn by the author :
             _take a look at the retinacolor.hpp module to discover Brice Chaix de Lavarene color mosaicing/demosaicing and the reference paper:
             B. Chaix de Lavarene, D. Alleysson, B. Durette, J. Herault (2007). "Efficient demosaicing through recursive filtering", IEEE International Conference on Image Processing ICIP 2007
             _take a look at imagelogpolprojection.hpp to discover retina spatial log sampling which originates from Barthelemy Durette phd with Jeanny Herault. A Retina / V1 cortex projection is also proposed and originates from Jeanny's discussions.
             more informations in the above cited Jeanny Heraults's book.
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public Retina (System.Drawing.Size inputSize);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="inputSize" Type="System.Drawing.Size" />
      </Parameters>
      <Docs>
        <param name="inputSize">The input frame size</param>
        <summary>
             Create a retina model
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public Retina (System.Drawing.Size inputSize, bool colorMode, Emgu.CV.Bioinspired.Retina.ColorSamplingMethod colorSamplingMethod, bool useRetinaLogSampling, double reductionFactor, double samplingStrength);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="inputSize" Type="System.Drawing.Size" />
        <Parameter Name="colorMode" Type="System.Boolean" />
        <Parameter Name="colorSamplingMethod" Type="Emgu.CV.Bioinspired.Retina+ColorSamplingMethod" />
        <Parameter Name="useRetinaLogSampling" Type="System.Boolean" />
        <Parameter Name="reductionFactor" Type="System.Double" />
        <Parameter Name="samplingStrength" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="inputSize">The input frame size</param>
        <param name="colorMode">Specifies if (true) color is processed of not (false) to then processing gray level image</param>
        <param name="colorSamplingMethod">Specifies which kind of color sampling will be used</param>
        <param name="useRetinaLogSampling">Activate retina log sampling, if true, the 2 following parameters can be used</param>
        <param name="reductionFactor">Only useful if param useRetinaLogSampling=true, specifies the reduction factor of the output frame (as the center (fovea) is high resolution and corners can be underscaled, then a reduction of the output is allowed without precision leak</param>
        <param name="samplingStrength">Only useful if param useRetinaLogSampling=true, specifies the strenght of the log scale that is applied</param>
        <summary>
             Create a retina model
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ClearBuffers">
      <MemberSignature Language="C#" Value="public void ClearBuffers ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Clear all retina buffers (equivalent to opening the eyes after a long period of eye close.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="DisposeObject">
      <MemberSignature Language="C#" Value="protected override void DisposeObject ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Release all unmanaged memory associated with the retina model.
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetMagno">
      <MemberSignature Language="C#" Value="public void GetMagno (Emgu.CV.IOutputArray magno);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="magno" Type="Emgu.CV.IOutputArray" />
      </Parameters>
      <Docs>
        <param name="magno">To be added.</param>
        <summary>
             Accessors of the motion channel of the retina (models peripheral vision)
             </summary>
        <returns>The motion channel of the retina.</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetParvo">
      <MemberSignature Language="C#" Value="public void GetParvo (Emgu.CV.IOutputArray parvo);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="parvo" Type="Emgu.CV.IOutputArray" />
      </Parameters>
      <Docs>
        <param name="parvo">To be added.</param>
        <summary>
             Accessors of the details channel of the retina (models foveal vision)
             </summary>
        <returns>The details channel of the retina.</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Parameters">
      <MemberSignature Language="C#" Value="public Emgu.CV.Bioinspired.Retina.RetinaParameters Parameters { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Bioinspired.Retina+RetinaParameters</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get or Set the Retina parameters.
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Run">
      <MemberSignature Language="C#" Value="public void Run (Emgu.CV.IInputArray image);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="image" Type="Emgu.CV.IInputArray" />
      </Parameters>
      <Docs>
        <param name="image">The input image to be processed</param>
        <summary>
             Method which allows retina to be applied on an input image, after run, encapsulated retina module is ready to deliver its outputs using dedicated acccessors. <seealso cref="M:Emgu.CV.Bioinspired.Retina.GetParvo(Emgu.CV.IOutputArray)" /> and <seealso cref="M:Emgu.CV.Bioinspired.Retina.GetMagno(Emgu.CV.IOutputArray)" /></summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
