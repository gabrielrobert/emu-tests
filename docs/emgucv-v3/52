<Type Name="MatND&lt;TDepth&gt;" FullName="Emgu.CV.MatND&lt;TDepth&gt;">
  <TypeSignature Language="C#" Value="public class MatND&lt;TDepth&gt; : Emgu.CV.CvArray&lt;TDepth&gt;, IEquatable&lt;Emgu.CV.MatND&lt;TDepth&gt;&gt; where TDepth : new()" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <TypeParameters>
    <TypeParameter Name="TDepth">
      <Constraints>
        <ParameterAttribute>DefaultConstructorConstraint</ParameterAttribute>
      </Constraints>
    </TypeParameter>
  </TypeParameters>
  <Base>
    <BaseTypeName>Emgu.CV.CvArray&lt;TDepth&gt;</BaseTypeName>
    <BaseTypeArguments>
      <BaseTypeArgument TypeParamName="TDepth">TDepth</BaseTypeArgument>
    </BaseTypeArguments>
  </Base>
  <Interfaces>
    <Interface>
      <InterfaceName>System.IEquatable&lt;Emgu.CV.MatND&lt;TDepth&gt;&gt;</InterfaceName>
    </Interface>
  </Interfaces>
  <Docs>
    <typeparam name="TDepth">The type of depth</typeparam>
    <summary>
             A MatND is a wrapper to cvMatND of OpenCV. 
             </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public MatND (int[] sizes);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="sizes" Type="System.Int32[]">
          <Attributes>
            <Attribute>
              <AttributeName>System.ParamArray</AttributeName>
            </Attribute>
          </Attributes>
        </Parameter>
      </Parameters>
      <Docs>
        <param name="sizes">The size for each dimension</param>
        <summary>
             Create a N-dimensional matrix 
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public MatND (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="info" Type="System.Runtime.Serialization.SerializationInfo" />
        <Parameter Name="context" Type="System.Runtime.Serialization.StreamingContext" />
      </Parameters>
      <Docs>
        <param name="info">The serialization info</param>
        <param name="context">The streaming context</param>
        <summary>
             Constructor used to deserialize runtime serialized object
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="AllocateData">
      <MemberSignature Language="C#" Value="protected override void AllocateData (int rows, int cols, int numberOfChannels);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="rows" Type="System.Int32" />
        <Parameter Name="cols" Type="System.Int32" />
        <Parameter Name="numberOfChannels" Type="System.Int32" />
      </Parameters>
      <Docs>
        <param name="rows">Not implemented</param>
        <param name="cols">Not implemented</param>
        <param name="numberOfChannels">Not implemented</param>
        <summary>
             This function is not implemented for MatND
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Convert&lt;TOtherDepth&gt;">
      <MemberSignature Language="C#" Value="public Emgu.CV.MatND&lt;TOtherDepth&gt; Convert&lt;TOtherDepth&gt; () where TOtherDepth : new();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.MatND&lt;TOtherDepth&gt;</ReturnType>
      </ReturnValue>
      <TypeParameters>
        <TypeParameter Name="TOtherDepth">
          <Constraints>
            <ParameterAttribute>DefaultConstructorConstraint</ParameterAttribute>
          </Constraints>
        </TypeParameter>
      </TypeParameters>
      <Parameters />
      <Docs>
        <typeparam name="TOtherDepth">The depth type to convert to</typeparam>
        <summary>
             Convert this matrix to different depth
             </summary>
        <returns>Matrix of different depth</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="CvDepth">
      <MemberSignature Language="C#" Value="protected static Emgu.CV.CvEnum.DepthType CvDepth { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.CvEnum.DepthType</ReturnType>
      </ReturnValue>
      <Docs>
        <summary> Get the depth representation for openCV</summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="DeserializeObjectData">
      <MemberSignature Language="C#" Value="protected override void DeserializeObjectData (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="info" Type="System.Runtime.Serialization.SerializationInfo" />
        <Parameter Name="context" Type="System.Runtime.Serialization.StreamingContext" />
      </Parameters>
      <Docs>
        <param name="info">Serialization info</param>
        <param name="context">Streaming context</param>
        <summary>
             A function used for runtime deserailization of the object
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="DisposeObject">
      <MemberSignature Language="C#" Value="protected override void DisposeObject ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Release the matrix and all the memory associate with it
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Equals">
      <MemberSignature Language="C#" Value="public bool Equals (Emgu.CV.MatND&lt;TDepth&gt; other);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Boolean</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="other" Type="Emgu.CV.MatND&lt;TDepth&gt;" />
      </Parameters>
      <Docs>
        <param name="other">The other MatND to compares to</param>
        <summary>
             Check if the two MatND are equal
             </summary>
        <returns>True if the two MatND equals</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="GetObjectData">
      <MemberSignature Language="C#" Value="public override void GetObjectData (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="info" Type="System.Runtime.Serialization.SerializationInfo" />
        <Parameter Name="context" Type="System.Runtime.Serialization.StreamingContext" />
      </Parameters>
      <Docs>
        <param name="info">Serialization info</param>
        <param name="context">Streaming context</param>
        <summary>
             A function used for runtime serialization of the object
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ManagedArray">
      <MemberSignature Language="C#" Value="public override Array ManagedArray { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Array</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get the underneath managed array
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="MCvMatND">
      <MemberSignature Language="C#" Value="public Emgu.CV.Structure.MCvMatND MCvMatND { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Structure.MCvMatND</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             The MCvMatND structure
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="NumberOfChannels">
      <MemberSignature Language="C#" Value="public override int NumberOfChannels { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Int32</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             This function is not implemented for MatND
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ReadXml">
      <MemberSignature Language="C#" Value="public override void ReadXml (System.Xml.XmlReader reader);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="reader" Type="System.Xml.XmlReader" />
      </Parameters>
      <Docs>
        <param name="reader">The XmlReader</param>
        <summary>
             Not Implemented
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="WriteXml">
      <MemberSignature Language="C#" Value="public override void WriteXml (System.Xml.XmlWriter writer);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Void</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="writer" Type="System.Xml.XmlWriter" />
      </Parameters>
      <Docs>
        <param name="writer">The XmlWriter</param>
        <summary>
             Not Implemented
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
