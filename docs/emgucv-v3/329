<Type Name="Bgr" FullName="Emgu.CV.Structure.Bgr">
  <TypeSignature Language="C#" Value="public struct Bgr : Emgu.CV.IColor, IEquatable&lt;Emgu.CV.Structure.Bgr&gt;" />
  <AssemblyInfo>
    <AssemblyName>Emgu.CV.World.IOS</AssemblyName>
    <AssemblyVersion>0.0.0.0</AssemblyVersion>
  </AssemblyInfo>
  <Base>
    <BaseTypeName>System.ValueType</BaseTypeName>
  </Base>
  <Interfaces>
    <Interface>
      <InterfaceName>Emgu.CV.IColor</InterfaceName>
    </Interface>
    <Interface>
      <InterfaceName>System.IEquatable&lt;Emgu.CV.Structure.Bgr&gt;</InterfaceName>
    </Interface>
  </Interfaces>
  <Attributes>
    <Attribute>
      <AttributeName>Emgu.CV.ColorInfo(ConversionCodename="Bgr")</AttributeName>
    </Attribute>
  </Attributes>
  <Docs>
    <summary> 
            Defines a Bgr (Blue Green Red) color
            </summary>
    <remarks>To be added.</remarks>
  </Docs>
  <Members>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public Bgr (System.Drawing.Color winColor);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="winColor" Type="System.Drawing.Color" />
      </Parameters>
      <Docs>
        <param name="winColor">System.Drawing.Color</param>
        <summary>
             Create a Bgr color using the System.Drawing.Color
             </summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName=".ctor">
      <MemberSignature Language="C#" Value="public Bgr (double blue, double green, double red);" />
      <MemberType>Constructor</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <Parameters>
        <Parameter Name="blue" Type="System.Double" />
        <Parameter Name="green" Type="System.Double" />
        <Parameter Name="red" Type="System.Double" />
      </Parameters>
      <Docs>
        <param name="blue"> The blue value for this color </param>
        <param name="green"> The green value for this color </param>
        <param name="red"> The red value for this color </param>
        <summary> Create a BGR color using the specific values</summary>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Blue">
      <MemberSignature Language="C#" Value="public double Blue { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary> Get or set the intensity of the blue color channel </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Dimension">
      <MemberSignature Language="C#" Value="public int Dimension { get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Int32</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get the dimension of this color
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Equals">
      <MemberSignature Language="C#" Value="public bool Equals (Emgu.CV.Structure.Bgr other);" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Boolean</ReturnType>
      </ReturnValue>
      <Parameters>
        <Parameter Name="other" Type="Emgu.CV.Structure.Bgr" />
      </Parameters>
      <Docs>
        <param name="other">The other color to compare with</param>
        <summary>
             Return true if the two color equals
             </summary>
        <returns>true if the two color equals</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Green">
      <MemberSignature Language="C#" Value="public double Green { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary> Get or set the intensity of the green color channel </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="MCvScalar">
      <MemberSignature Language="C#" Value="public Emgu.CV.Structure.MCvScalar MCvScalar { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>Emgu.CV.Structure.MCvScalar</ReturnType>
      </ReturnValue>
      <Docs>
        <summary>
             Get or Set the equivalent MCvScalar value
             </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="Red">
      <MemberSignature Language="C#" Value="public double Red { set; get; }" />
      <MemberType>Property</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.Double</ReturnType>
      </ReturnValue>
      <Docs>
        <summary> Get or set the intensity of the red color channel </summary>
        <value>To be added.</value>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
    <Member MemberName="ToString">
      <MemberSignature Language="C#" Value="public override string ToString ();" />
      <MemberType>Method</MemberType>
      <AssemblyInfo>
        <AssemblyVersion>0.0.0.0</AssemblyVersion>
      </AssemblyInfo>
      <ReturnValue>
        <ReturnType>System.String</ReturnType>
      </ReturnValue>
      <Parameters />
      <Docs>
        <summary>
             Represent this color as a String
             </summary>
        <returns>The string representation of this color</returns>
        <remarks>To be added.</remarks>
      </Docs>
    </Member>
  </Members>
</Type>
