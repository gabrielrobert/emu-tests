﻿//----------------------------------------------------------------------------
//  Copyright (C) 2004-2017 by EMGU Corporation. All rights reserved.       
//----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using CoreGraphics;
using CoreImage;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using UIKit;

namespace Example.iOS
{
    public class HdrProcessingViewController : UIViewController
    {
        private readonly Dictionary<string, float> ImageNames = new Dictionary<string, float>
        {
            // SET 1
            {"IMG_0163.JPG", 3.2f},
            {"IMG_0164.JPG", 2f},
            {"IMG_0165.JPG", 1f},
            {"IMG_0166.JPG", 1 / 2f},
            {"IMG_0167.JPG", 1 / 8f},
            {"IMG_0168.JPG", 1 / 30f},
            {"IMG_0169.JPG", 1 / 125f},
            {"IMG_0170.JPG", 1 / 500f},
            {"IMG_0171.JPG", 1 / 2000f}

            // SET 2
            //{"IMG_0395.JPG", 1 / 3f},
            //{"IMG_0345.JPG", 1 / 4167f},
            //{"IMG_0347.JPG", 1 / 2037f},
            //{"IMG_0349.JPG", 1 / 1623f},
            //{"IMG_0351.JPG", 1 / 1259f},
            //{"IMG_0353.JPG", 1 / 1008f},
            //{"IMG_0355.JPG", 1 / 805f},
            //{"IMG_0357.JPG", 1 / 669f},
            //{"IMG_0359.JPG", 1 / 501f},
            //{"IMG_0361.JPG", 1 / 400f},
            //{"IMG_0363.JPG", 1 / 323f},
            //{"IMG_0365.JPG", 1 / 251f},
            //{"IMG_0367.JPG", 1 / 200f},
            //{"IMG_0369.JPG", 1 / 160f},
            //{"IMG_0371.JPG", 1 / 125f},
            //{"IMG_0373.JPG", 1 / 100f},
            //{"IMG_0375.JPG", 1 / 80f},
            //{"IMG_0377.JPG", 1 / 62f},
            //{"IMG_0379.JPG", 1 / 50f},
            //{"IMG_0381.JPG", 1 / 40f},
            //{"IMG_0383.JPG", 1 / 30f},
            //{"IMG_0385.JPG", 1 / 20f},
            //{"IMG_0387.JPG", 1 / 20f},
            //{"IMG_0389.JPG", 1 / 15f},
            //{"IMG_0391.JPG", 1 / 5f},
            //{"IMG_0393.JPG", 1 / 5f},

            // SET 3 - REFER TO https://www.ccoderun.ca/programming/doxygen/opencv/tutorial_py_hdr.html
            //{"StLouisArchMultExpEV+1.51.JPG", 2.5f},
            //{"StLouisArchMultExpEV+4.09.JPG", 15f},
            //{"StLouisArchMultExpEV-1.82.JPG", 0.25f},
            //{"StLouisArchMultExpEV-4.72.JPG", 0.0333f},
        };

        public UIImageView imageView = new UIImageView(new CGRect(10, 190, 300, 200));
        public UIImage sourceImage;
        public UISlider sliderBrightness = new UISlider(new CGRect(100, 70, 210, 20));
        public UISlider sliderSaturation = new UISlider(new CGRect(100, 110, 210, 20));
        public UISlider sliderContrast = new UISlider(new CGRect(100, 150, 210, 20));

        public override void ViewDidLoad()
        {
            SetControls();

            var debevecCalibrater = new CalibrateDebevec();
            var robertsonCalibrater = new CalibrateRobertson();

            var mergeDebevec = new MergeDebevec();
            var mergeRobertson = new MergeRobertson();
            var mergeMertens = new MergeMertens();

            var tonemapDurand = new TonemapDurand(2.2f);
            var tonemapMantiuk = new TonemapMantiuk();

            base.ViewDidLoad();

            if (AppDelegate.iOS7Plus)
            {
                EdgesForExtendedLayout = UIRectEdge.None;
            }

            using (var imagesVector = new VectorOfMat())
            {
                using (var timesVector = new VectorOfFloat())
                {
                    foreach (var image in ImageNames.OrderBy(x => x.Value))
                    {
                        imagesVector.Push(new Mat(image.Key));
                        timesVector.Push(new[] { image.Value });
                    }

                    // Based on http://docs.opencv.org/3.3.0/d3/db7/tutorial_hdr_imaging.html
                    // AND https://www.ccoderun.ca/programming/doxygen/opencv/tutorial_py_hdr.html

                    //Mat response;
                    //Ptr<CalibrateDebevec> calibrate = createCalibrateDebevec();
                    //calibrate->process(images, response, times);
                    var response = new Mat();
                    var calibrate = new CalibrateDebevec();
                    calibrate.Process(imagesVector, response, timesVector);

                    //Mat hdr;
                    //Ptr<MergeDebevec> merge_debevec = createMergeDebevec();
                    //merge_debevec->process(images, hdr, times, response);
                    var hdr = new Mat();
                    var merge_debevec = new MergeDebevec();
                    merge_debevec.Process(imagesVector, hdr, timesVector, response);

                    //Mat fusion;
                    //Ptr<MergeMertens> merge_mertens = createMergeMertens();
                    //merge_mertens->process(images, fusion);
                    var fusion = new Mat();
                    var merge_mertens = new MergeMertens();
                    merge_mertens.Process(imagesVector, fusion, timesVector, response);

                    var matrix = new Matrix<float>(fusion.Rows, fusion.Cols, fusion.NumberOfChannels);
                    fusion.CopyTo(matrix);
                    matrix._Mul(255);

                    var matrixImage = matrix.Mat.ToImage<Bgr, byte>();

                    // TESTS

                    var new_image = new Mat(matrixImage.Size, DepthType.Default, matrixImage.NumberOfChannels);
                    /// Do the operation new_image(i,j) = alpha*image(i,j) + beta
                    var alpha = 1;
                    var beta = 2;
                    for (var y = 0; y < matrixImage.Rows; y++)
                    {
                        for (var x = 0; x < matrixImage.Cols; x++)
                        {
                            for (var c = 0; c < 3; c++)
                            {
                                matrix[y, x] = alpha * matrix[y, x] + beta;
                            }
                        }
                    }

                    //Matrix<Byte> myMatrix = new Matrix<Byte>();
                    //CvInvoke.LUT(image, myMatrix, test);

                    CvInvoke.Imwrite("fusion.png", matrix);
                    CvInvoke.Imwrite("fusion.hdr", matrix);

                    sourceImage = matrix.Mat.ToImage<Bgr, byte>().ToUIImage();
                    imageView.Image = sourceImage;
                    View.Add(imageView);
                }
            }
        }

        private void SetControls()
        {

            View.Add(sliderContrast);
            View.Add(sliderSaturation);
            View.Add(sliderBrightness);

            sliderSaturation.MinValue = 0;
            sliderSaturation.MaxValue = 2;
            sliderBrightness.MinValue = -1;
            sliderBrightness.MaxValue = 1;
            sliderContrast.MinValue = 0;
            sliderContrast.MaxValue = 4;

            // set default values
            sliderSaturation.Value = 1;
            sliderBrightness.Value = 0;
            sliderContrast.Value = 1;

            sliderContrast.ValueChanged += HandleValueChanged;
            sliderSaturation.ValueChanged += HandleValueChanged;
            sliderBrightness.ValueChanged += HandleValueChanged;
        }

        private void HandleValueChanged(object sender, EventArgs e)
        {
            var context = CIContext.FromOptions(null);
            var colorCtrls = new CIColorControls { Image = CIImage.FromCGImage(sourceImage.CGImage) };

            // use the low-res version
            if (colorCtrls == null)
            {
                colorCtrls = new CIColorControls { Image = CIImage.FromCGImage(sourceImage.CGImage) };
            }
            else
            {
                colorCtrls.Image = CIImage.FromCGImage(sourceImage.CGImage);
            }

            // set the values
            colorCtrls.Brightness = sliderBrightness.Value;
            colorCtrls.Saturation = sliderSaturation.Value;
            colorCtrls.Contrast = sliderContrast.Value;
            // do the transformation
            using (var outputImage = colorCtrls.OutputImage)
            {
                var result = context.CreateCGImage(outputImage, outputImage.Extent);
                // display the result in the UIImageView
                imageView.Image = UIImage.FromImage(result);
            }
        }
    }
}